# Migrations
migrations-create:
	@read -p "Name of the migration: " migration \
	&& echo "Create migrations $$migration at twitter" \
	&& goose -dir migrations postgres "dbname=twitter user=postgres password=postgres sslmode=disable connect_timeout=5 statement_timeout=60000 host=localhost port=5432" create $$migration sql

migrations-up:
	@goose -dir migrations postgres "dbname=twitter user=postgres password=postgres sslmode=disable connect_timeout=5 statement_timeout=60000 host=localhost port=5432" up

migrations-down:
	@goose -dir migrations postgres "dbname=twitter user=postgres password=postgres sslmode=disable connect_timeout=5 statement_timeout=60000 host=localhost port=5432" down
