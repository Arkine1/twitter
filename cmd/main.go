package main

import (
	"database/sql"
	"fmt"
	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
	"gitlab.com/Arkine1/twitter/internal/config"
	"gitlab.com/Arkine1/twitter/internal/handler"
	"gitlab.com/Arkine1/twitter/internal/logic"
	"gitlab.com/Arkine1/twitter/internal/repo"
)

func main() {
	cfg := config.NewConfig()

	db, err := sql.Open(
		"postgres",
		cfg.DatabaseURL)
	if err != nil {
		panic(err)
	}

	r := repo.NewRepository(db)

	l := logic.NewLogic(r)

	h := handler.NewHandler(l)

	router := setupRouter(h)

	router.Run(fmt.Sprintf(":%s", cfg.Port))
}

func setupRouter(h *handler.Handler) *gin.Engine {
	r := gin.Default()

	// Ping test
	r.GET("/ping", h.Ping)

	return r
}
