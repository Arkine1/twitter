package config

import (
	"github.com/BurntSushi/toml"
	"log"
)

const configPath = "internal/config/config.toml"

type Config struct {
	Port        string `toml:"port"`
	DatabaseURL string `toml:"database_url"`
}

func NewConfig() *Config {
	config := &Config{}
	_, err := toml.DecodeFile(configPath, config)
	if err != nil {
		log.Fatal(err)
	}

	return config
}
