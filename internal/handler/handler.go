package handler

import (
	"gitlab.com/Arkine1/twitter/internal/logic"
)

type Handler struct {
	logic *logic.Logic
}

func NewHandler(l *logic.Logic) *Handler {
	return &Handler{
		logic: l,
	}
}
