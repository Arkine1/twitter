package handler

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/Arkine1/twitter/internal/model"
	"net/http"
	"time"
)

func (h *Handler) Ping(c *gin.Context) {
	ping := model.Ping{
		CreatedAt: time.Now(),
	}

	err := h.logic.CreatePing(c.Request.Context(), ping)
	if err != nil {
		c.JSON(http.StatusInternalServerError, model.ItemResponse{
			Code:  http.StatusInternalServerError,
			Error: err.Error(),
		},
		)
	}

	c.JSON(http.StatusOK, model.ItemResponse{
		Code: http.StatusOK,
		Item: nil,
	})
}
