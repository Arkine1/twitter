package repo

import (
	"context"
	"database/sql"
	"gitlab.com/Arkine1/twitter/internal/model"
)

type PingRepository interface {
	Create(ctx context.Context, item model.Ping) error
}

func NewPingRepository(db *sql.DB) PingRepository {
	return &PingRepo{
		db: db,
	}
}

type PingRepo struct {
	db *sql.DB
}

func (repo *PingRepo) Create(ctx context.Context, item model.Ping) error {
	query := `INSERT INTO ping
	(created_at) VALUES($1);`

	_, err := repo.db.ExecContext(ctx,
		query,
		item.CreatedAt,
	)
	if err != nil {
		return err
	}

	return nil
}
