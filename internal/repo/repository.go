package repo

import "database/sql"

type Repository struct {
	PingRepository
}

func NewRepository(db *sql.DB) *Repository {
	return &Repository{PingRepository: NewPingRepository(db)}
}
