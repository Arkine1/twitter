package model

type ItemResponse struct {
	Code  int64       `json:"code"`
	Item  interface{} `json:"item,omitempty"`
	Error string      `json:"error,omitempty"`
}
