package logic

import (
	"context"
	"gitlab.com/Arkine1/twitter/internal/model"
)

func (l *Logic) CreatePing(ctx context.Context, item model.Ping) error {
	err := l.repo.Create(ctx, item)
	if err != nil {
		return err
	}

	return nil
}
