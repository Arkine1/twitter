package logic

import (
	"gitlab.com/Arkine1/twitter/internal/repo"
)

type Logic struct {
	repo *repo.Repository
}

func NewLogic(repo *repo.Repository) *Logic {
	return &Logic{
		repo: repo,
	}
}
